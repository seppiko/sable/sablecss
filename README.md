<p align="center">
  <a href="https://sablecss.seppiko.org" target="_blank">
    <picture>
      <img alt="SableCSS" src="https://sablecss.seppiko.org/images/logo.svg" width="150" style="max-width: 100%;">
    </picture>
  </a>
</p>

<p align="center">
A CSS builder for functionalize packed.
</p>

<p align="center">
  <a href="https://gitlab.com/seppiko/sable/sablecss/-/blob/main/LICENSE">
    <img src="https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square" alt="License">
  </a>
  <a class="text-decoration-none" href="https://srl.cx/sablecss">
    <img src="https://img.shields.io/npm/v/@seppiko/sablecss?style=flat-square" alt="Latest Release">
  </a>
  <a class="text-decoration-none" href="https://srl.cx/sablecss">
    <img src="https://img.shields.io/npm/dt/@seppiko/sablecss?style=flat-square" alt="Total Downloads">
  </a>
</p>

## Install

```shell
npm i -D @seppiko/sablecss
```

## Used

Try [Seppiko SableCSS Normalize and Common][1] with CDN.

[1]: https://github.com/seppiko/sablecss

## Sponsors

<a href="https://www.jetbrains.com/" target="_blank"><img src="https://seppiko.org/images/jetbrains.png" alt="JetBrians" width="100" /></a>
